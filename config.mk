$(call inherit-product, vendor/gapps/common-blobs.mk)

# Include package overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += vendor/gapps/overlay
DEVICE_PACKAGE_OVERLAYS += \
    vendor/gapps/overlay/common/

# framework
PRODUCT_PACKAGES += \
    com.google.android.maps

ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    com.google.android.dialer.support
endif

# app
ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    PrebuiltBugle
endif

ifeq ($(TARGET_INCLUDE_STOCK_ARCORE),true)
PRODUCT_PACKAGES += \
    arcore
endif

# System app
PRODUCT_PACKAGES += \
    GoogleExtShared \
    GooglePrintRecommendationService

# System priv-app
PRODUCT_PACKAGES += \
    GoogleExtServicesPrebuilt \
    GooglePackageInstaller \
    GooglePermissionControllerPrebuilt

# Product app
ifneq ($(TARGET_USES_LIGHT_GAPPS), true)
PRODUCT_PACKAGES += \
    CalendarGooglePrebuilt \
    GoogleTTS \
    Photos \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    talkback
endif
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    Chrome \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    LatinIMEGooglePrebuilt \
    LocationHistoryPrebuilt \
    MarkupGoogle \
    SoundPickerPrebuilt \
    WebViewGoogle

# Product priv-app
ifneq ($(TARGET_USES_LIGHT_GAPPS), true)
PRODUCT_PACKAGES += \
    Velvet
endif
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    CarrierServices \
    ConfigUpdater \
    ConnMetrics \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    MatchmakerPrebuilt \
    Phonesky \
    PixelSetupWizard \
    PrebuiltGmsCoreQt \
    SetupWizardPrebuilt \
    TurboPrebuilt \
    WellbeingPrebuilt

ifeq ($(IS_PHONE),true)
PRODUCT_PACKAGES += \
    GoogleDialer
endif

ifneq ($(TARGET_USES_LIGHT_GAPPS), true)
# Recorder
ifneq ($(TARGET_SUPPORTS_GOOGLE_RECORDER), false)
PRODUCT_PACKAGES += \
    NgaResources \
    RecorderPrebuilt
endif
endif

# Disable unwanted stuff in PA.
PRODUCT_PACKAGES += \
    PixelSetupWizardOverlay
